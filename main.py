import numpy as np
import pandas as pd
from PIL import Image
import matplotlib.pyplot as plt
import os
from keras import preprocessing
from keras.models import Sequential
from keras.layers import Conv2D, Dropout, Dense, Flatten, Conv2DTranspose, BatchNormalization, LeakyReLU, Reshape
import tensorflow as tf
from tensorflow import summary
import os
from keras.callbacks import TensorBoard

from eda import Dataset
import cv2


def show_nine_images(imgdata):
    horizontal_1 = np.concatenate((imgdata[0], imgdata[1], imgdata[2]), axis=1)
    horizontal_2 = np.concatenate((imgdata[3], imgdata[4], imgdata[5]), axis=1)
    horizontal_3 = np.concatenate((imgdata[6], imgdata[7], imgdata[8]), axis=1)
    vertical = np.concatenate((horizontal_1, horizontal_2, horizontal_3), axis=0)
    cv2.imshow("Examples", vertical)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


dataset = Dataset()
print(dataset.df.head())
images = [cv2.imread(dataset.images_path + path) for path in dataset.df['image_id']]
print(images[0])
images = cv2.resize(images, (64, 64))

show_nine_images(images)

X_train = images

# _______GENERATOR_________

noise_shape = 100

generator = Sequential()
generator.add(Dense(4 * 4 * 512, input_shape=[noise_shape]))
generator.add(Reshape([4, 4, 512]))
generator.add(Conv2DTranspose(256, kernel_size=4, strides=2, padding="same"))
generator.add(LeakyReLU(alpha=0.2))
generator.add(BatchNormalization())
generator.add(Conv2DTranspose(128, kernel_size=4, strides=2, padding="same"))
generator.add(LeakyReLU(alpha=0.2))
generator.add(BatchNormalization())
generator.add(Conv2DTranspose(64, kernel_size=4, strides=2, padding="same"))
generator.add(LeakyReLU(alpha=0.2))
generator.add(BatchNormalization())
generator.add(Conv2DTranspose(3, kernel_size=4, strides=2, padding="same",
                              activation='sigmoid'))

# _______DYSKRYMINATOR_________
discriminator = Sequential()
discriminator.add(Conv2D(32, kernel_size=4, strides=2, padding="same", input_shape=[64, 64, 3]))
discriminator.add(Conv2D(64, kernel_size=4, strides=2, padding="same"))
discriminator.add(LeakyReLU(0.2))
discriminator.add(BatchNormalization())
discriminator.add(Conv2D(128, kernel_size=4, strides=2, padding="same"))
discriminator.add(LeakyReLU(0.2))
discriminator.add(BatchNormalization())
discriminator.add(Conv2D(256, kernel_size=4, strides=2, padding="same"))
discriminator.add(LeakyReLU(0.2))
discriminator.add(Flatten())
discriminator.add(Dropout(0.5))
discriminator.add(Dense(1, activation='sigmoid'))


# ŁĄCZENIE MODELI:

GAN = Sequential([generator, discriminator])

discriminator.compile(optimizer='adam', loss='binary_crossentropy')
discriminator.trainable = False

GAN.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])

print('GAN layers: ', GAN.layers)
print(GAN.summary())

epochs = 300
batch_size = 128

# lista do zapisywania lossu dla dyskryminatora:
D_loss = []
# lista do zapisywania lossu dla generatora:
G_loss = []

log_path = './logs'
callback = TensorBoard(log_path)
callback.set_model(GAN)


with tf.device('/gpu:0'):
    for epoch in range(epochs):
        print(f"Aktualnie trenowany EPOCH:  {epoch + 1}")

        # For every batch in the dataset
        for i in range(X_train.shape[0] // batch_size):
            if i % 100 == 0:
                print(f"\tNumer batcha: {i} z: {len(X_train) // batch_size}")
            # Generujemy szum dla generatora:
            noise = np.random.uniform(-1, 1, size=[batch_size, noise_shape])
            # Predykcja - czyli generator generuje coś z tego szumu
            gen_image = generator.predict_on_batch(noise)
            train_dataset = X_train[i * batch_size:(i + 1) * batch_size]
            # train on real image
            train_label = np.ones(shape=(batch_size, 1))
            discriminator.trainable = True
            d_loss1 = discriminator.train_on_batch(train_dataset, train_label)

            # train on fake image
            train_label = np.zeros(shape=(batch_size, 1))
            d_loss2 = discriminator.train_on_batch(gen_image, train_label)

            noise = np.random.uniform(-1, 1, size=[batch_size, noise_shape])
            train_label = np.ones(shape=(batch_size, 1))
            discriminator.trainable = False
            # train the generator
            g_loss = GAN.train_on_batch(noise, train_label)
            D_loss.append(d_loss1 + d_loss2)
            G_loss.append(g_loss)

        if epoch % 5 == 0:
            samples = 9
            x_fake = generator.predict(np.random.normal(loc=0, scale=1, size=(samples, 100)))
            show_nine_images(x_fake)

        print('Epoch: %d,  Loss: D_real = %.3f, D_fake = %.3f,  G = %.3f' % (epoch + 1, d_loss1, d_loss2, g_loss))
print('Training is complete')
